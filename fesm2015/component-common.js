import { Component, Input, Output, EventEmitter, NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TimepickerComponent {
    constructor() {
        this.showSeccond = false;
        this.timepickerModelChange = new EventEmitter();
        this.spinners = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    getDateSelected() {
        // if(this.objectUtils.isValid(this.timepickerItem)){
        //   return new Date(this.timepickerItem.year, this.timepickerItem.month - 1, this.timepickerItem.day);
        // }
        // return null;
    }
    /**
     * @param {?} time
     * @return {?}
     */
    timeSelect(time) {
        this.timepickerModelChange.emit(time);
    }
}
TimepickerComponent.decorators = [
    { type: Component, args: [{
                selector: "win-timepicker",
                template: "<div id=\"win-timepicker-test\">\n    <ngb-timepicker [(ngModel)]=\"timepickerModel\"  (ngModelChange)=\"timeSelect($event)\" [seconds]=\"showSeccond\" [spinners]=\"spinners\"></ngb-timepicker>\n</div>\n",
                styles: ["#win-datepicker{width:100%;border:1px solid #ebebeb;border-radius:3px;display:flex;flex-direction:column;background:#fff;cursor:pointer}#win-datepicker .datepicker-container{display:flex;list-style:none;margin:0;padding:5px 10px;flex:1;color:#999}#win-datepicker .datepicker-container li{margin:0}#win-datepicker .datepicker-container .datepicker{flex:1;display:flex;justify-content:baseline;align-items:center}#win-datepicker .datepicker-container .datepicker img{width:20px;margin-right:5px}#win-datepicker .datepicker-container .datepicker span{flex:8}#win-datepicker .datepicker-container .datepicker span.dropdown-arrow{font-size:24px;flex:1;text-align:right}#win-datepicker .input-datepicker{width:100%;height:0;visibility:hidden;line-height:0;font-size:0;padding:0;margin:0}"]
            }] }
];
/** @nocollapse */
TimepickerComponent.ctorParameters = () => [];
TimepickerComponent.propDecorators = {
    timepickerModel: [{ type: Input }],
    showSeccond: [{ type: Input }],
    timepickerModelChange: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TimepickerModule {
}
TimepickerModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TimepickerComponent],
                imports: [
                    NgbModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                exports: [TimepickerComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { TimepickerComponent, TimepickerModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LWNvbW1vbi5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vY29tcG9uZW50LWNvbW1vbi9jb21wb25lbnRzL3RpbWVwaWNrZXIvdGltZXBpY2tlci50cyIsIm5nOi8vY29tcG9uZW50LWNvbW1vbi9jb21wb25lbnRzL3RpbWVwaWNrZXIvdGltZXBpY2tlci5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOZ2JUaW1lU3RydWN0IH0gZnJvbSBcIkBuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ3aW4tdGltZXBpY2tlclwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL3RpbWVwaWNrZXIuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vdGltZXBpY2tlci5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIFRpbWVwaWNrZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSB0aW1lcGlja2VyTW9kZWw6IE5nYlRpbWVTdHJ1Y3Q7XG4gIEBJbnB1dCgpIHNob3dTZWNjb25kOiBib29sZWFuID0gZmFsc2U7XG4gIEBPdXRwdXQoKSB0aW1lcGlja2VyTW9kZWxDaGFuZ2U6IEV2ZW50RW1pdHRlcjxOZ2JUaW1lU3RydWN0PiA9IG5ldyBFdmVudEVtaXR0ZXI8TmdiVGltZVN0cnVjdD4oKTtcbiAgc3Bpbm5lcnMgPSBmYWxzZTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQgeyBcbiAgfVxuXG4gIGdldERhdGVTZWxlY3RlZCgpOiB2b2lke1xuICAgIC8vIGlmKHRoaXMub2JqZWN0VXRpbHMuaXNWYWxpZCh0aGlzLnRpbWVwaWNrZXJJdGVtKSl7XG4gICAgLy8gICByZXR1cm4gbmV3IERhdGUodGhpcy50aW1lcGlja2VySXRlbS55ZWFyLCB0aGlzLnRpbWVwaWNrZXJJdGVtLm1vbnRoIC0gMSwgdGhpcy50aW1lcGlja2VySXRlbS5kYXkpO1xuICAgIC8vIH1cbiAgICAvLyByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHRpbWVTZWxlY3QodGltZTogTmdiVGltZVN0cnVjdCk6IHZvaWR7XG4gICAgdGhpcy50aW1lcGlja2VyTW9kZWxDaGFuZ2UuZW1pdCh0aW1lKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFRpbWVwaWNrZXJDb21wb25lbnQgfSBmcm9tICcuL3RpbWVwaWNrZXInO1xuaW1wb3J0IHsgTmdiTW9kdWxlIH0gZnJvbSBcIkBuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwXCI7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtUaW1lcGlja2VyQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIE5nYk1vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtUaW1lcGlja2VyQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBUaW1lcGlja2VyTW9kdWxlIHsgfVxuIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsTUFRYSxtQkFBbUI7SUFNOUI7UUFKUyxnQkFBVyxHQUFZLEtBQUssQ0FBQztRQUM1QiwwQkFBcUIsR0FBZ0MsSUFBSSxZQUFZLEVBQWlCLENBQUM7UUFDakcsYUFBUSxHQUFHLEtBQUssQ0FBQztLQUdoQjs7OztJQUVELFFBQVE7S0FDUDs7OztJQUVELGVBQWU7Ozs7O0tBS2Q7Ozs7O0lBRUQsVUFBVSxDQUFDLElBQW1CO1FBQzVCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDdkM7OztZQTFCRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjtnQkFDMUIsdU5BQWdDOzthQUVqQzs7Ozs7OEJBRUUsS0FBSzswQkFDTCxLQUFLO29DQUNMLE1BQU07Ozs7Ozs7QUNYVCxNQWNhLGdCQUFnQjs7O1lBVDVCLFFBQVEsU0FBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztnQkFDbkMsT0FBTyxFQUFFO29CQUNQLFNBQVM7b0JBQ1QsV0FBVztvQkFDWCxtQkFBbUI7aUJBQ3BCO2dCQUNELE9BQU8sRUFBRSxDQUFDLG1CQUFtQixDQUFDO2FBQy9COzs7Ozs7Ozs7Ozs7Ozs7In0=