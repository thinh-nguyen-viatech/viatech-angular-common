import { EventEmitter, OnInit } from "@angular/core";
import { NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";
export declare class TimepickerComponent implements OnInit {
    timepickerModel: NgbTimeStruct;
    showSeccond: boolean;
    timepickerModelChange: EventEmitter<NgbTimeStruct>;
    spinners: boolean;
    constructor();
    ngOnInit(): void;
    getDateSelected(): void;
    timeSelect(time: NgbTimeStruct): void;
}
