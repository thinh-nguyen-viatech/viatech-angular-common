(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@ng-bootstrap/ng-bootstrap'), require('@angular/forms')) :
    typeof define === 'function' && define.amd ? define('component-common', ['exports', '@angular/core', '@ng-bootstrap/ng-bootstrap', '@angular/forms'], factory) :
    (factory((global['component-common'] = {}),global.ng.core,global.ngBootstrap,global.ng.forms));
}(this, (function (exports,core,ngBootstrap,forms) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TimepickerComponent = /** @class */ (function () {
        function TimepickerComponent() {
            this.showSeccond = false;
            this.timepickerModelChange = new core.EventEmitter();
            this.spinners = false;
        }
        /**
         * @return {?}
         */
        TimepickerComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        /**
         * @return {?}
         */
        TimepickerComponent.prototype.getDateSelected = /**
         * @return {?}
         */
            function () {
                // if(this.objectUtils.isValid(this.timepickerItem)){
                //   return new Date(this.timepickerItem.year, this.timepickerItem.month - 1, this.timepickerItem.day);
                // }
                // return null;
            };
        /**
         * @param {?} time
         * @return {?}
         */
        TimepickerComponent.prototype.timeSelect = /**
         * @param {?} time
         * @return {?}
         */
            function (time) {
                this.timepickerModelChange.emit(time);
            };
        TimepickerComponent.decorators = [
            { type: core.Component, args: [{
                        selector: "win-timepicker",
                        template: "<div id=\"win-timepicker-test\">\n    <ngb-timepicker [(ngModel)]=\"timepickerModel\"  (ngModelChange)=\"timeSelect($event)\" [seconds]=\"showSeccond\" [spinners]=\"spinners\"></ngb-timepicker>\n</div>\n",
                        styles: ["#win-datepicker{width:100%;border:1px solid #ebebeb;border-radius:3px;display:flex;flex-direction:column;background:#fff;cursor:pointer}#win-datepicker .datepicker-container{display:flex;list-style:none;margin:0;padding:5px 10px;flex:1;color:#999}#win-datepicker .datepicker-container li{margin:0}#win-datepicker .datepicker-container .datepicker{flex:1;display:flex;justify-content:baseline;align-items:center}#win-datepicker .datepicker-container .datepicker img{width:20px;margin-right:5px}#win-datepicker .datepicker-container .datepicker span{flex:8}#win-datepicker .datepicker-container .datepicker span.dropdown-arrow{font-size:24px;flex:1;text-align:right}#win-datepicker .input-datepicker{width:100%;height:0;visibility:hidden;line-height:0;font-size:0;padding:0;margin:0}"]
                    }] }
        ];
        /** @nocollapse */
        TimepickerComponent.ctorParameters = function () { return []; };
        TimepickerComponent.propDecorators = {
            timepickerModel: [{ type: core.Input }],
            showSeccond: [{ type: core.Input }],
            timepickerModelChange: [{ type: core.Output }]
        };
        return TimepickerComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TimepickerModule = /** @class */ (function () {
        function TimepickerModule() {
        }
        TimepickerModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [TimepickerComponent],
                        imports: [
                            ngBootstrap.NgbModule,
                            forms.FormsModule,
                            forms.ReactiveFormsModule
                        ],
                        exports: [TimepickerComponent]
                    },] }
        ];
        return TimepickerModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.TimepickerComponent = TimepickerComponent;
    exports.TimepickerModule = TimepickerModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LWNvbW1vbi51bWQuanMubWFwIiwic291cmNlcyI6WyJuZzovL2NvbXBvbmVudC1jb21tb24vY29tcG9uZW50cy90aW1lcGlja2VyL3RpbWVwaWNrZXIudHMiLCJuZzovL2NvbXBvbmVudC1jb21tb24vY29tcG9uZW50cy90aW1lcGlja2VyL3RpbWVwaWNrZXIubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTmdiVGltZVN0cnVjdCB9IGZyb20gXCJAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcFwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwid2luLXRpbWVwaWNrZXJcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi90aW1lcGlja2VyLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3RpbWVwaWNrZXIuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBUaW1lcGlja2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgdGltZXBpY2tlck1vZGVsOiBOZ2JUaW1lU3RydWN0O1xuICBASW5wdXQoKSBzaG93U2VjY29uZDogYm9vbGVhbiA9IGZhbHNlO1xuICBAT3V0cHV0KCkgdGltZXBpY2tlck1vZGVsQ2hhbmdlOiBFdmVudEVtaXR0ZXI8TmdiVGltZVN0cnVjdD4gPSBuZXcgRXZlbnRFbWl0dGVyPE5nYlRpbWVTdHJ1Y3Q+KCk7XG4gIHNwaW5uZXJzID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHsgXG4gIH1cblxuICBnZXREYXRlU2VsZWN0ZWQoKTogdm9pZHtcbiAgICAvLyBpZih0aGlzLm9iamVjdFV0aWxzLmlzVmFsaWQodGhpcy50aW1lcGlja2VySXRlbSkpe1xuICAgIC8vICAgcmV0dXJuIG5ldyBEYXRlKHRoaXMudGltZXBpY2tlckl0ZW0ueWVhciwgdGhpcy50aW1lcGlja2VySXRlbS5tb250aCAtIDEsIHRoaXMudGltZXBpY2tlckl0ZW0uZGF5KTtcbiAgICAvLyB9XG4gICAgLy8gcmV0dXJuIG51bGw7XG4gIH1cblxuICB0aW1lU2VsZWN0KHRpbWU6IE5nYlRpbWVTdHJ1Y3QpOiB2b2lke1xuICAgIHRoaXMudGltZXBpY2tlck1vZGVsQ2hhbmdlLmVtaXQodGltZSk7XG4gIH1cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBUaW1lcGlja2VyQ29tcG9uZW50IH0gZnJvbSAnLi90aW1lcGlja2VyJztcbmltcG9ydCB7IE5nYk1vZHVsZSB9IGZyb20gXCJAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcFwiO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbVGltZXBpY2tlckNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBOZ2JNb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbVGltZXBpY2tlckNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVGltZXBpY2tlck1vZHVsZSB7IH1cbiJdLCJuYW1lcyI6WyJFdmVudEVtaXR0ZXIiLCJDb21wb25lbnQiLCJJbnB1dCIsIk91dHB1dCIsIk5nTW9kdWxlIiwiTmdiTW9kdWxlIiwiRm9ybXNNb2R1bGUiLCJSZWFjdGl2ZUZvcm1zTW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7UUFjRTtZQUpTLGdCQUFXLEdBQVksS0FBSyxDQUFDO1lBQzVCLDBCQUFxQixHQUFnQyxJQUFJQSxpQkFBWSxFQUFpQixDQUFDO1lBQ2pHLGFBQVEsR0FBRyxLQUFLLENBQUM7U0FHaEI7Ozs7UUFFRCxzQ0FBUTs7O1lBQVI7YUFDQzs7OztRQUVELDZDQUFlOzs7WUFBZjs7Ozs7YUFLQzs7Ozs7UUFFRCx3Q0FBVTs7OztZQUFWLFVBQVcsSUFBbUI7Z0JBQzVCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkM7O29CQTFCRkMsY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7d0JBQzFCLHVOQUFnQzs7cUJBRWpDOzs7OztzQ0FFRUMsVUFBSztrQ0FDTEEsVUFBSzs0Q0FDTEMsV0FBTTs7UUFtQlQsMEJBQUM7S0EzQkQ7Ozs7OztBQ0hBO1FBS0E7U0FTaUM7O29CQVRoQ0MsYUFBUSxTQUFDO3dCQUNSLFlBQVksRUFBRSxDQUFDLG1CQUFtQixDQUFDO3dCQUNuQyxPQUFPLEVBQUU7NEJBQ1BDLHFCQUFTOzRCQUNUQyxpQkFBVzs0QkFDWEMseUJBQW1CO3lCQUNwQjt3QkFDRCxPQUFPLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztxQkFDL0I7O1FBQytCLHVCQUFDO0tBVGpDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsifQ==