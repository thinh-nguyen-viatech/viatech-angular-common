/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from "@angular/core";
export class TimepickerComponent {
    constructor() {
        this.showSeccond = false;
        this.timepickerModelChange = new EventEmitter();
        this.spinners = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    getDateSelected() {
        // if(this.objectUtils.isValid(this.timepickerItem)){
        //   return new Date(this.timepickerItem.year, this.timepickerItem.month - 1, this.timepickerItem.day);
        // }
        // return null;
    }
    /**
     * @param {?} time
     * @return {?}
     */
    timeSelect(time) {
        this.timepickerModelChange.emit(time);
    }
}
TimepickerComponent.decorators = [
    { type: Component, args: [{
                selector: "win-timepicker",
                template: "<div id=\"win-timepicker-test\">\n    <ngb-timepicker [(ngModel)]=\"timepickerModel\"  (ngModelChange)=\"timeSelect($event)\" [seconds]=\"showSeccond\" [spinners]=\"spinners\"></ngb-timepicker>\n</div>\n",
                styles: ["#win-datepicker{width:100%;border:1px solid #ebebeb;border-radius:3px;display:flex;flex-direction:column;background:#fff;cursor:pointer}#win-datepicker .datepicker-container{display:flex;list-style:none;margin:0;padding:5px 10px;flex:1;color:#999}#win-datepicker .datepicker-container li{margin:0}#win-datepicker .datepicker-container .datepicker{flex:1;display:flex;justify-content:baseline;align-items:center}#win-datepicker .datepicker-container .datepicker img{width:20px;margin-right:5px}#win-datepicker .datepicker-container .datepicker span{flex:8}#win-datepicker .datepicker-container .datepicker span.dropdown-arrow{font-size:24px;flex:1;text-align:right}#win-datepicker .input-datepicker{width:100%;height:0;visibility:hidden;line-height:0;font-size:0;padding:0;margin:0}"]
            }] }
];
/** @nocollapse */
TimepickerComponent.ctorParameters = () => [];
TimepickerComponent.propDecorators = {
    timepickerModel: [{ type: Input }],
    showSeccond: [{ type: Input }],
    timepickerModelChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    TimepickerComponent.prototype.timepickerModel;
    /** @type {?} */
    TimepickerComponent.prototype.showSeccond;
    /** @type {?} */
    TimepickerComponent.prototype.timepickerModelChange;
    /** @type {?} */
    TimepickerComponent.prototype.spinners;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXBpY2tlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbXBvbmVudC1jb21tb24vIiwic291cmNlcyI6WyJjb21wb25lbnRzL3RpbWVwaWNrZXIvdGltZXBpY2tlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQztBQVEvRSxNQUFNLE9BQU8sbUJBQW1CO0lBTTlCO1FBSlMsZ0JBQVcsR0FBWSxLQUFLLENBQUM7UUFDNUIsMEJBQXFCLEdBQWdDLElBQUksWUFBWSxFQUFpQixDQUFDO1FBQ2pHLGFBQVEsR0FBRyxLQUFLLENBQUM7SUFHakIsQ0FBQzs7OztJQUVELFFBQVE7SUFDUixDQUFDOzs7O0lBRUQsZUFBZTtRQUNiLHFEQUFxRDtRQUNyRCx1R0FBdUc7UUFDdkcsSUFBSTtRQUNKLGVBQWU7SUFDakIsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsSUFBbUI7UUFDNUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7WUExQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLHVOQUFnQzs7YUFFakM7Ozs7OzhCQUVFLEtBQUs7MEJBQ0wsS0FBSztvQ0FDTCxNQUFNOzs7O0lBRlAsOENBQXdDOztJQUN4QywwQ0FBc0M7O0lBQ3RDLG9EQUFpRzs7SUFDakcsdUNBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOZ2JUaW1lU3RydWN0IH0gZnJvbSBcIkBuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ3aW4tdGltZXBpY2tlclwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL3RpbWVwaWNrZXIuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vdGltZXBpY2tlci5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIFRpbWVwaWNrZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSB0aW1lcGlja2VyTW9kZWw6IE5nYlRpbWVTdHJ1Y3Q7XG4gIEBJbnB1dCgpIHNob3dTZWNjb25kOiBib29sZWFuID0gZmFsc2U7XG4gIEBPdXRwdXQoKSB0aW1lcGlja2VyTW9kZWxDaGFuZ2U6IEV2ZW50RW1pdHRlcjxOZ2JUaW1lU3RydWN0PiA9IG5ldyBFdmVudEVtaXR0ZXI8TmdiVGltZVN0cnVjdD4oKTtcbiAgc3Bpbm5lcnMgPSBmYWxzZTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQgeyBcbiAgfVxuXG4gIGdldERhdGVTZWxlY3RlZCgpOiB2b2lke1xuICAgIC8vIGlmKHRoaXMub2JqZWN0VXRpbHMuaXNWYWxpZCh0aGlzLnRpbWVwaWNrZXJJdGVtKSl7XG4gICAgLy8gICByZXR1cm4gbmV3IERhdGUodGhpcy50aW1lcGlja2VySXRlbS55ZWFyLCB0aGlzLnRpbWVwaWNrZXJJdGVtLm1vbnRoIC0gMSwgdGhpcy50aW1lcGlja2VySXRlbS5kYXkpO1xuICAgIC8vIH1cbiAgICAvLyByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHRpbWVTZWxlY3QodGltZTogTmdiVGltZVN0cnVjdCk6IHZvaWR7XG4gICAgdGhpcy50aW1lcGlja2VyTW9kZWxDaGFuZ2UuZW1pdCh0aW1lKTtcbiAgfVxufVxuIl19